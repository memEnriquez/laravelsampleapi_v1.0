<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{

	public function index()
	{
		return response()->json(User::all());
	}

	public function show(User $user)
	{
		return response()->json($user, 200);
	}

	public function store(Request $request)
	{
		// temporary password for now
		$user = User::create( $request->all() + ['password' => bcrypt('secret')] );

		return response()->json($user, 201);
	}

	public function update(Request $request, User $user)
	{
		$user->update( $request->all() + ['password' => bcrypt('secret')] );

		return response()->json($user, 200);
	}

	public function delete(Request $request, User $user)
	{
		$user->delete();

		return response()->json(null, 204);
	}
}
